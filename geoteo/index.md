# Matteo's headquarters

![](pics/island.png)

> - [How I manage my desktop with Qtile](config.html)
> - [Some unfinished scribbles on Computability Theory](notes.html)
> - [Wiener attack, a simple way to brake the RSA Cryptosystem](attack.html)
> - [Yet another Asteroids clone but way cooler](astro.html)
