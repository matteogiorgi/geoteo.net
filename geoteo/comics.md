# Matteo's comics

![](pics/administration.png)

> You may have noticed some funny comics here and there through these pages but unfortunately they're not my work. Anyhow you can still find them inside the (in)famous [The UNIX-HATERS handbook](https://web.mit.edu/~simsong/www/ugh.pdf) from 1994 by Garfinkel, Weise and Strassmann.
