# Matteo's contacts

![](pics/mail.png)

> - [`github.com/matteogiorgi` $~$ <i class="fas fa-share-alt"></i>](https://github.com/matteogiorgi)
> - [`matteo.giorgi@protonmail.com` $~$ <i class="far fa-envelope-open"></i>](mailto:matteo.giorgi@protonmail.com)
> - [`t.me/geoteodotnet` $~$ <i class="far fa-comment"></i>](https://t.me/geoteodotnet)
