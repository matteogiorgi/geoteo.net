# Matteo's personal info

![](pics/evolution.png)

> - [Full Curriculum Vitae $~$ <i class="fas fa-signature"></i>](https://matteogiorgi.github.io/cv/src/cv.pdf)
> - [UniPi exams transcript $~$ <i class="fas fa-chart-line"></i>](pics/exams.pdf)
> - [Language certificates $~$ <i class="fas fa-spell-check"></i>](pics/cert.pdf)
