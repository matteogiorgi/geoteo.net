# Qtile window-manager

I've been using [Qtile](http://www.qtile.org/) for a while now and it's a fantastic window manager. Just like [dwm](https://dwm.suckless.org), [Xmonad](https://xmonad.org) or [Awesome](https://awesomewm.org/), it organises its windows dynamically keeping them in a stack with predefined layouts so the user can choose the layout he prefers best and let the wm do the dirty work.

Qtile has its source code and config file written in Python, plus a neat [documentation](http://docs.qtile.org/en/latest) comes in support if you fancy to play with it. To have a rough idea on how it works, you can browse through my [dotfiles](https://github.com/MatteoGiorgi/.dotfiles) or just take a look at the sample below.

![](pics/qtile.mp4){ width=100% }
